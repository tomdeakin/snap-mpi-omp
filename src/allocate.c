
#include <stdlib.h>

#include "problem.h"
#include "allocate.h"

void allocate_memory(struct problem * problem, struct rankinfo * rankinfo, struct memory * memory)
{
    // Angular flux
    for (int i = 0; i < 8; i++)
    {
        memory->angular_flux_in[i] =
            malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);
        memory->angular_flux_out[i] =
            malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);
    }

    // Allocate edge arrays
    memory->flux_i = malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->ny*rankinfo->nz);
    memory->flux_j = malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->nx*rankinfo->nz);
    memory->flux_k = malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->nx*rankinfo->ny);

    // Scalar flux
    // grid * ng
    memory->scalar_flux = malloc(sizeof(double)*rankinfo->nx*rankinfo->ny*rankinfo->nz*problem->ng);
    memory->old_inner_scalar_flux = malloc(sizeof(double)*rankinfo->nx*rankinfo->ny*rankinfo->nz*problem->ng);
    memory->old_outer_scalar_flux = malloc(sizeof(double)*rankinfo->nx*rankinfo->ny*rankinfo->nz*problem->ng);

    //Scalar flux moments
    if (problem->cmom-1 == 0)
        memory->scalar_flux_moments = NULL;
    else
        memory->scalar_flux_moments = malloc(sizeof(double)*(problem->cmom-1)*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);

    // Cosine coefficients
    memory->mu = malloc(sizeof(double)*problem->nang);
    memory->eta = malloc(sizeof(double)*problem->nang);
    memory->xi = malloc(sizeof(double)*problem->nang);

    // Diamond difference weights
    memory->dd_i = malloc(sizeof(double));
    memory->dd_j = malloc(sizeof(double)*problem->nang);
    memory->dd_k = malloc(sizeof(double)*problem->nang);

    // Material cross section
    memory->mat_cross_section = malloc(sizeof(double)*problem->ng);

    // Quadrature set
    memory->quad_weights = malloc(sizeof(double)*problem->nang);

    // Scattering co-efficients
    memory->scat_coeff = malloc(sizeof(double)*problem->nang*problem->cmom*8);

    memory->scattering_matrix = malloc(sizeof(double)*problem->nmom*problem->ng*problem->ng);

    // Velocities
    memory->velocities = malloc(sizeof(double)*problem->ng);
    memory->velocity_delta = malloc(sizeof(double)*problem->ng);


    // Sources
    memory->fixed_source = malloc(sizeof(double)*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);
    memory->outer_source = malloc(sizeof(double)*problem->cmom*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);
    memory->inner_source = malloc(sizeof(double)*problem->cmom*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);

    memory->denominator = malloc(sizeof(double)*problem->nang*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz);


}

void free_memory(struct memory * memory)
{
    for (int i = 0; i < 8; i++)
    {
        free(memory->angular_flux_in[i]);
        free(memory->angular_flux_out[i]);
    }
    free(memory->flux_i);
    free(memory->flux_j);
    free(memory->flux_k);
    free(memory->scalar_flux);
    free(memory->old_inner_scalar_flux);
    free(memory->old_outer_scalar_flux);
    if (memory->scalar_flux_moments)
        free(memory->scalar_flux_moments);
    free(memory->quad_weights);
    free(memory->mu);
    free(memory->eta);
    free(memory->xi);
    free(memory->dd_i);
    free(memory->dd_j);
    free(memory->dd_k);
    free(memory->mat_cross_section);
    free(memory->scat_coeff);
    free(memory->scattering_matrix);
    free(memory->velocities);
    free(memory->velocity_delta);
    free(memory->denominator);
    free(memory->fixed_source);
    free(memory->inner_source);
    free(memory->outer_source);
}


void swap_angular_flux_buffers(struct memory * memory)
{
    for (int i = 0; i < 8; i++)
    {
        double *tmp;
        tmp = memory->angular_flux_in[i];
        memory->angular_flux_in[i] = memory->angular_flux_out[i];
        memory->angular_flux_out[i] = tmp;
    }
}
