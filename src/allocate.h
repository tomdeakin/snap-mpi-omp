
/** \file
* \brief Manage the allocation of arrays
*
* All problem scope arrays are allocated in the host DRAM using these functions calls.
*/

#pragma once

#include "global.h"

/** \brief Struct to hold the buffers
*
* All the memory arrays are stored here
*/
struct memory
{
    /** @{ \brief
    Angular flux - two copies for time dependence, each ocant in own buffer
    */
    double * angular_flux_in[8];
    double * angular_flux_out[8];
    /** @} */

	/**@{*/
	/** \brief Edge flux arrays */
	/** Size: (nang, ng, ny, nz)
	*
	* Note, rankinfo spatial dimension
	*/
	double *flux_i;
	/** \brief Edge flux arrays */
	/** Size: (nang, ng, nx, nz)
	*
	* Note, rankinfo spatial dimension
	*/
	double *flux_j;
	/** \brief Edge flux arrays */
	/** Size: (nang, ng, nx, ny)
	*
	* Note, rankinfo spatial dimension
	*/
	double *flux_k;
	/**@}*/

	/**@{ \brief Scalar flux */
	/**
	* Size: (ng, nx, ny, nz)
	*
	* Note, rankinfo spatial dimensions
	*/
	double *scalar_flux;
	double *old_inner_scalar_flux;
	double *old_outer_scalar_flux;
	/**@}*/

    /** \brief Quadrature weights */
    double * quad_weights;

	/**@{*/
	/** \brief Scalar flux moments */
	/** Size: (cmom-1, ng, nx, ny, nz)
	*
	* Note, rankinfo spatial dimensions
	*/
	double *scalar_flux_moments;
	/**@}*/

	/**@{ \brief Cosine coefficients */
	/** Size: (nang) */
	double *mu;
	double *eta;
	double *xi;
	/**@}*/

    /** \brief Scattering coefficient */
    double * scat_coeff;

    /** \brief Scattering terms */
    double * scattering_matrix;

    /** @{ \brief Diamond diference co-efficients */
    double * dd_i;
    double * dd_j;
    double * dd_k;
    /** @} */

    /** \brief Mock velocities array */
    double * velocities;

    /** \brief Time absorption coefficient */
    double * velocity_delta;

    /** \brief Transport denominator */
    double * denominator;

	/** \brief Material cross sections
	*
	* ASSUME ONE MATERIAL
	*
	* Size: (ng)
	*/
	double *mat_cross_section;

    /** @{ \brief Source terms */
    double * fixed_source;
    double * outer_source;
    double * inner_source;
    /** @} */


};

/** \brief Allocate the problem arrays */
void allocate_memory(struct problem * problem, struct rankinfo * rankinfo, struct memory * memory);

/** \brief Free the arrays sroted in the \a mem struct */
void free_memory(struct memory * memory);

/** \brief Swap the angular flux pointers around (in <-> out) */
void swap_angular_flux_buffers(struct memory * memory);
