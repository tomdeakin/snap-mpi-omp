
#pragma once

/** \file
* \brief Scalar flux reduction routines
*/

#define RED_BLOCK_SIZE 4

#include <math.h>
#include <string.h>

#include "global.h"
#include "allocate.h"
#include "kernels.h"
#include "profiler.h"


/** \brief Enqueue kernel to compute scalar flux (non-blocking) */
void compute_scalar_flux(struct problem * problem, struct rankinfo * rankinfo, struct memory * memory);

/** \brief Enqueue kernel to compute scalar flux moments (non-blocking) */
void compute_scalar_flux_moments(struct problem * problem, struct rankinfo * rankinfo, struct memory * memory);


/** \brief Copy the scalar flux back to the host (choose blocking) */
void copy_back_scalar_flux(struct problem *problem, struct rankinfo * rankinfo, double * restrict dst, const double * restrict src);

