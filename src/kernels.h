
#pragma once

/** \file
* \brief Contains function declarations of kernels
*/

#include "sweep.h"


void calc_dd_coeff(
    const unsigned int nang,
    const double dx,
    const double dy,
    const double dz,
    const double * restrict eta,
    const double * restrict xi,
    double * restrict dd_i,
    double * restrict dd_j,
    double * restrict dd_k
);


void calc_denominator(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,
    const double * restrict mat_cross_section,
    const double * restrict velocity_delta,
    const double * restrict mu,
    const double * restrict dd_i,
    const double * restrict dd_j,
    const double * restrict dd_k,
    double * restrict denominator
);


void calc_velocity_delta(
    const unsigned int ng,
    const double * restrict velocities,
    const double dt,
    double * restrict velocity_delta
);


void calc_inner_source(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int ng,
    const unsigned int cmom,
    const unsigned int nmom,
    const double * restrict outer_source,
    const double * restrict scattering_matrix,
    const double * restrict scalar_flux,
    const double * restrict scalar_flux_moments,
    double * restrict inner_source
);


void calc_outer_source(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int ng,
    const unsigned int cmom,
    const unsigned int nmom,
    const double * restrict fixed_source,
    const double * restrict scattering_matrix,
    const double * restrict scalar_flux,
    const double * restrict scalar_flux_moments,
    double * restrict outer_source
);



void reduce_flux(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,

    const double * restrict angular_flux_in_0,
    const double * restrict angular_flux_in_1,
    const double * restrict angular_flux_in_2,
    const double * restrict angular_flux_in_3,
    const double * restrict angular_flux_in_4,
    const double * restrict angular_flux_in_5,
    const double * restrict angular_flux_in_6,
    const double * restrict angular_flux_in_7,

    const double * restrict angular_flux_out_0,
    const double * restrict angular_flux_out_1,
    const double * restrict angular_flux_out_2,
    const double * restrict angular_flux_out_3,
    const double * restrict angular_flux_out_4,
    const double * restrict angular_flux_out_5,
    const double * restrict angular_flux_out_6,
    const double * restrict angular_flux_out_7,

    const double * restrict velocity_delta,
    const double * restrict quad_weights,

    double * restrict scalar_flux
);



void reduce_flux_moments(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,
    const unsigned int cmom,

    const double * restrict angular_flux_in_0,
    const double * restrict angular_flux_in_1,
    const double * restrict angular_flux_in_2,
    const double * restrict angular_flux_in_3,
    const double * restrict angular_flux_in_4,
    const double * restrict angular_flux_in_5,
    const double * restrict angular_flux_in_6,
    const double * restrict angular_flux_in_7,

    const double * restrict angular_flux_out_0,
    const double * restrict angular_flux_out_1,
    const double * restrict angular_flux_out_2,
    const double * restrict angular_flux_out_3,
    const double * restrict angular_flux_out_4,
    const double * restrict angular_flux_out_5,
    const double * restrict angular_flux_out_6,
    const double * restrict angular_flux_out_7,

    const double * restrict velocity_delta,
    const double * restrict quad_weights,
    const double * restrict scat_coeff,

    double * restrict scalar_flux_moments
);


void sweep_plane_kernel(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,
    const unsigned int cmom,
    const int istep,
    const int jstep,
    const int kstep,
    const unsigned int oct,
    const unsigned int z_pos,
    const unsigned int num_planes,
    const struct cell_id * plane,
    const double * restrict source,
    const double * restrict scat_coeff,
    const double * restrict dd_i,
    const double * restrict dd_j,
    const double * restrict dd_k,
    const double * restrict mu,
    const double * restrict velocity_delta,
    const double * restrict mat_cross_section,
    const double * restrict denominator,
    const double * restrict angular_flux_in,
    double * restrict flux_i,
    double * restrict flux_j,
    double * restrict flux_k,
    double * restrict angular_flux_out
);

