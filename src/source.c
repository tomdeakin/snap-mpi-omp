
#include "source.h"


void compute_outer_source(
    const struct problem * problem,
    const struct rankinfo * rankinfo,
    struct memory * memory
    )
{

    calc_outer_source(
        rankinfo->nx, rankinfo->ny, rankinfo->nz,
        problem->ng, problem->cmom, problem->nmom,
        memory->fixed_source, memory->scattering_matrix,
        memory->scalar_flux, memory->scalar_flux_moments,
        memory->outer_source
    );

}


void compute_inner_source(
    const struct problem * problem,
    const struct rankinfo * rankinfo,
    struct memory * memory
    )
{

    calc_inner_source(
        rankinfo->nx, rankinfo->ny, rankinfo->nz,
        problem->ng, problem->cmom, problem->nmom,
        memory->outer_source, memory->scattering_matrix,
        memory->scalar_flux, memory->scalar_flux_moments,
        memory->inner_source
    );
}

