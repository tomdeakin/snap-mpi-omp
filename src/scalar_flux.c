
#include "scalar_flux.h"

void compute_scalar_flux(
    struct problem * problem,
    struct rankinfo * rankinfo,
    struct memory * memory
    )
{

    reduce_flux(
        rankinfo->nx, rankinfo->ny, rankinfo->nz,
        problem->nang, problem->ng,
        memory->angular_flux_in[0],
        memory->angular_flux_in[1],
        memory->angular_flux_in[2],
        memory->angular_flux_in[3],
        memory->angular_flux_in[4],
        memory->angular_flux_in[5],
        memory->angular_flux_in[6],
        memory->angular_flux_in[7],
        memory->angular_flux_out[0],
        memory->angular_flux_out[1],
        memory->angular_flux_out[2],
        memory->angular_flux_out[3],
        memory->angular_flux_out[4],
        memory->angular_flux_out[5],
        memory->angular_flux_out[6],
        memory->angular_flux_out[7],
        memory->velocity_delta, memory->quad_weights,
        memory->scalar_flux
    );
}

void compute_scalar_flux_moments(
    struct problem * problem,
    struct rankinfo * rankinfo,
    struct memory * memory
    )
{

    reduce_flux_moments(
        rankinfo->nx, rankinfo->ny, rankinfo->nz,
        problem->nang, problem->ng, problem->cmom,
        memory->angular_flux_in[0],
        memory->angular_flux_in[1],
        memory->angular_flux_in[2],
        memory->angular_flux_in[3],
        memory->angular_flux_in[4],
        memory->angular_flux_in[5],
        memory->angular_flux_in[6],
        memory->angular_flux_in[7],
        memory->angular_flux_out[0],
        memory->angular_flux_out[1],
        memory->angular_flux_out[2],
        memory->angular_flux_out[3],
        memory->angular_flux_out[4],
        memory->angular_flux_out[5],
        memory->angular_flux_out[6],
        memory->angular_flux_out[7],
        memory->velocity_delta, memory->quad_weights,
        memory->scat_coeff,
        memory->scalar_flux_moments
    );

}


void copy_back_scalar_flux(
    struct problem *problem,
    struct rankinfo * rankinfo,
    double * restrict dst,
    const double * restrict src
    )
{
    memcpy(dst, src,
        sizeof(double)*problem->ng*rankinfo->nx*rankinfo->ny*rankinfo->nz
    );
}
