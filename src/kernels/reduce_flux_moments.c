
#define ANGULAR_FLUX_INDEX(a,g,i,j,k,nang,ng,nx,ny) ((a)+((nang)*(g))+((nang)*(ng)*(i))+((nang)*(ng)*(nx)*(j))+((nang)*(ng)*(nx)*(ny)*(k)))
#define SCALAR_FLUX_MOMENTS_INDEX(m,g,i,j,k,cmom,ng,nx,ny) ((m)+((cmom-1)*(g))+((cmom-1)*(ng)*(i))+((cmom-1)*(ng)*(nx)*(j))+((cmom-1)*(ng)*(nx)*(ny)*(k)))
#define SCAT_COEFF_INDEX(a,l,o,nang,cmom) ((a)+((nang)*(l))+((nang)*(cmom)*o))

#define angular_flux_in_0(a,g,i,j,k) angular_flux_in_0[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_1(a,g,i,j,k) angular_flux_in_1[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_2(a,g,i,j,k) angular_flux_in_2[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_3(a,g,i,j,k) angular_flux_in_3[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_4(a,g,i,j,k) angular_flux_in_4[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_5(a,g,i,j,k) angular_flux_in_5[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_6(a,g,i,j,k) angular_flux_in_6[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_7(a,g,i,j,k) angular_flux_in_7[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_0(a,g,i,j,k) angular_flux_out_0[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_1(a,g,i,j,k) angular_flux_out_1[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_2(a,g,i,j,k) angular_flux_out_2[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_3(a,g,i,j,k) angular_flux_out_3[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_4(a,g,i,j,k) angular_flux_out_4[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_5(a,g,i,j,k) angular_flux_out_5[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_6(a,g,i,j,k) angular_flux_out_6[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_7(a,g,i,j,k) angular_flux_out_7[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]

#define scalar_flux_moments(l,g,i,j,k) scalar_flux_moments[SCALAR_FLUX_MOMENTS_INDEX((l),(g),(i),(j),(k),cmom,ng,nx,ny)]
#define scat_coeff(a,l,o) scat_coeff[SCAT_COEFF_INDEX((a),(l),(o),nang,cmom)]


// We want to perform a weighted sum of angles in each cell in each energy group for each moment
// One work-group per cell per energy group, and reduce within a work-group
// Work-groups must be power of two sized
void reduce_flux_moments(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,
    const unsigned int cmom,

    const double * angular_flux_in_0,
    const double * angular_flux_in_1,
    const double * angular_flux_in_2,
    const double * angular_flux_in_3,
    const double * angular_flux_in_4,
    const double * angular_flux_in_5,
    const double * angular_flux_in_6,
    const double * angular_flux_in_7,

    const double * angular_flux_out_0,
    const double * angular_flux_out_1,
    const double * angular_flux_out_2,
    const double * angular_flux_out_3,
    const double * angular_flux_out_4,
    const double * angular_flux_out_5,
    const double * angular_flux_out_6,
    const double * angular_flux_out_7,

    const double * velocity_delta,
    const double * quad_weights,
    const double * scat_coeff,

    double * scalar_flux_moments
    )
{

#pragma omp parallel for collapse(5)
    for (unsigned int l = 0; l < cmom-1; l++)
        for (unsigned int k = 0; k < nz; k++)
            for (unsigned int j = 0; j < ny; j++)
                for (unsigned int i = 0; i < nx; i++)
                    for (unsigned int g = 0; g < ng; g++)
                    {
                        scalar_flux_moments(l,g,i,j,k) = 0.0;

                        for (unsigned int a = 0; a < nang; a++)
                        {
                            const double w = quad_weights[a];
                            if (velocity_delta[g] != 0.0)
                            {
                                scalar_flux_moments(l,g,i,j,k) +=
                                    scat_coeff(a,l+1,0) * w * (0.5 * (angular_flux_out_0(a,g,i,j,k) + angular_flux_in_0(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,1) * w * (0.5 * (angular_flux_out_1(a,g,i,j,k) + angular_flux_in_1(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,2) * w * (0.5 * (angular_flux_out_2(a,g,i,j,k) + angular_flux_in_2(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,3) * w * (0.5 * (angular_flux_out_3(a,g,i,j,k) + angular_flux_in_3(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,4) * w * (0.5 * (angular_flux_out_4(a,g,i,j,k) + angular_flux_in_4(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,5) * w * (0.5 * (angular_flux_out_5(a,g,i,j,k) + angular_flux_in_5(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,6) * w * (0.5 * (angular_flux_out_6(a,g,i,j,k) + angular_flux_in_6(a,g,i,j,k))) +
                                    scat_coeff(a,l+1,7) * w * (0.5 * (angular_flux_out_7(a,g,i,j,k) + angular_flux_in_7(a,g,i,j,k)));
                            }
                            else
                            {
                                scalar_flux_moments(l,g,i,j,k) +=
                                    scat_coeff(a,l+1,0) * w * angular_flux_out_0(a,g,i,j,k) +
                                    scat_coeff(a,l+1,1) * w * angular_flux_out_1(a,g,i,j,k) +
                                    scat_coeff(a,l+1,2) * w * angular_flux_out_2(a,g,i,j,k) +
                                    scat_coeff(a,l+1,3) * w * angular_flux_out_3(a,g,i,j,k) +
                                    scat_coeff(a,l+1,4) * w * angular_flux_out_4(a,g,i,j,k) +
                                    scat_coeff(a,l+1,5) * w * angular_flux_out_5(a,g,i,j,k) +
                                    scat_coeff(a,l+1,6) * w * angular_flux_out_6(a,g,i,j,k) +
                                    scat_coeff(a,l+1,7) * w * angular_flux_out_7(a,g,i,j,k);
                            }
                        } // End of angle loop
                    } // End of group loop

}
