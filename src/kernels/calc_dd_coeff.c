
void calc_dd_coeff(
    const unsigned int nang,
    const double dx,
    const double dy,
    const double dz,
    const double * restrict eta,
    const double * restrict xi,
    double * restrict dd_i,
    double * restrict dd_j,
    double * restrict dd_k
    )
{

    dd_i[0] = 2.0 / dx;

    for (unsigned int a = 0; a < nang; a++)
    {

        dd_j[a] = (2.0 / dy) * eta[a];
        dd_k[a] = (2.0 / dz) * xi[a];
    }
}
