
#define ANGULAR_FLUX_INDEX(a,g,i,j,k,nang,ng,nx,ny) ((a)+((nang)*(g))+((nang)*(ng)*(i))+((nang)*(ng)*(nx)*(j))+((nang)*(ng)*(nx)*(ny)*(k)))
#define SCALAR_FLUX_INDEX(g,i,j,k,ng,nx,ny) ((g)+((ng)*(i))+((ng)*(nx)*(j))+((ng)*(nx)*(ny)*(k)))


#define angular_flux_in_0(a,g,i,j,k) angular_flux_in_0[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_1(a,g,i,j,k) angular_flux_in_1[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_2(a,g,i,j,k) angular_flux_in_2[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_3(a,g,i,j,k) angular_flux_in_3[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_4(a,g,i,j,k) angular_flux_in_4[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_5(a,g,i,j,k) angular_flux_in_5[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_6(a,g,i,j,k) angular_flux_in_6[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_in_7(a,g,i,j,k) angular_flux_in_7[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_0(a,g,i,j,k) angular_flux_out_0[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_1(a,g,i,j,k) angular_flux_out_1[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_2(a,g,i,j,k) angular_flux_out_2[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_3(a,g,i,j,k) angular_flux_out_3[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_4(a,g,i,j,k) angular_flux_out_4[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_5(a,g,i,j,k) angular_flux_out_5[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_6(a,g,i,j,k) angular_flux_out_6[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define angular_flux_out_7(a,g,i,j,k) angular_flux_out_7[ANGULAR_FLUX_INDEX((a),(g),(i),(j),(k),nang,ng,nx,ny)]
#define scalar_flux(g,i,j,k) scalar_flux[SCALAR_FLUX_INDEX((g),(i),(j),(k),ng,nx,ny)]

// We want to perform a weighted sum of angles in each cell in each energy group
// One work-group per cell per energy group, and reduce within a work-group
// Work-groups must be power of two sized
void reduce_flux(
    const unsigned int nx,
    const unsigned int ny,
    const unsigned int nz,
    const unsigned int nang,
    const unsigned int ng,

    const double * restrict angular_flux_in_0,
    const double * restrict angular_flux_in_1,
    const double * restrict angular_flux_in_2,
    const double * restrict angular_flux_in_3,
    const double * restrict angular_flux_in_4,
    const double * restrict angular_flux_in_5,
    const double * restrict angular_flux_in_6,
    const double * restrict angular_flux_in_7,

    const double * restrict angular_flux_out_0,
    const double * restrict angular_flux_out_1,
    const double * restrict angular_flux_out_2,
    const double * restrict angular_flux_out_3,
    const double * restrict angular_flux_out_4,
    const double * restrict angular_flux_out_5,
    const double * restrict angular_flux_out_6,
    const double * restrict angular_flux_out_7,

    const double * restrict velocity_delta,
    const double * restrict quad_weights,

    double * restrict scalar_flux
    )
{
    // Reduce angles within in cell/group
#pragma omp parallel for collapse(4)
    for (unsigned int k = 0; k < nz; k++)
        for (unsigned int j = 0; j < ny; j++)
            for (unsigned int i = 0; i < nx; i++)
                for (unsigned int g = 0; g < ng; g++)
                {
                    scalar_flux(g,i,j,k) = 0.0;
                    for (unsigned int a = 0; a < nang; a++)
                    {
                        const double w = quad_weights[a];
                        if (velocity_delta[g] != 0.0)
                        {
                            scalar_flux(g,i,j,k) +=
                                w * (0.5 * (angular_flux_out_0(a,g,i,j,k) + angular_flux_in_0(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_1(a,g,i,j,k) + angular_flux_in_1(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_2(a,g,i,j,k) + angular_flux_in_2(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_3(a,g,i,j,k) + angular_flux_in_3(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_4(a,g,i,j,k) + angular_flux_in_4(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_5(a,g,i,j,k) + angular_flux_in_5(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_6(a,g,i,j,k) + angular_flux_in_6(a,g,i,j,k))) +
                                w * (0.5 * (angular_flux_out_7(a,g,i,j,k) + angular_flux_in_7(a,g,i,j,k)));
                        }
                        else
                        {
                            scalar_flux(g,i,j,k) +=
                                w * angular_flux_out_0(a,g,i,j,k) +
                                w * angular_flux_out_1(a,g,i,j,k) +
                                w * angular_flux_out_2(a,g,i,j,k) +
                                w * angular_flux_out_3(a,g,i,j,k) +
                                w * angular_flux_out_4(a,g,i,j,k) +
                                w * angular_flux_out_5(a,g,i,j,k) +
                                w * angular_flux_out_6(a,g,i,j,k) +
                                w * angular_flux_out_7(a,g,i,j,k);
                        }
                    }
                }

}
