
// Calculate the time absorbtion coefficient
void calc_velocity_delta(
    const unsigned int ng,
    const double * restrict velocities,
    const double dt,
    double * restrict velocity_delta
    )
{
    for (unsigned int g = 0; g < ng; g++)
        velocity_delta[g] = 2.0 / (dt * velocities[g]);
}
