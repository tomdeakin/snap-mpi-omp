
#include "profiler.h"

double wtime(void)
{
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec * 1.0E-6;
}

void zero_timers(struct timers * timers)
{
    timers->setup_time = 0.0;
    timers->outer_source_time = 0.0;
    timers->inner_source_time = 0.0;
    timers->sweep_time = 0.0;
    timers->reduction_time = 0.0;
    timers->simulation_time = 0.0;
    timers->convergence_time = 0.0;
    timers->outer_params_time = 0.0;
}


void outer_profiler(struct timers * timers)
{
    if (!profiling)
        return;

    return;
}



void inner_profiler(struct timers * timers, struct problem * problem)
{
    if (!profiling)
        return;

    return;
}


void chunk_profiler(struct timers * timers)
{
    if (!profiling)
        return;

    return;
}


